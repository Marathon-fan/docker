
# how to use it


## 1 image 1(debian with vim and git)  
$> docker pull marathonfan/debian:1.01

## 2 image 2(distributed lookup service)  
$> docker pull marathonfan/dockerapp

## 3 section introduction  
section2: working with docker images  
section3: create containerized web applications  
section4: docker networking  
section5: create a continuous integration pipeline  
section6: deploy docker containers in production  


